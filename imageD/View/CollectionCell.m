//
//  CollectionCell.m
//  imageD
//
//  Created by Павел Зорин on 24.04.2020.
//  Copyright © 2020 Zorin. All rights reserved.
//

#import "CollectionCell.h"

@interface CollectionCell ()

@end

@implementation CollectionCell

@synthesize imageView;
@synthesize indicatorLoad;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self setupCell];
    }
    return self;
}

-(void) setupCell
{
    indicatorLoad = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [indicatorLoad startAnimating];
    [self.contentView addSubview:indicatorLoad];
    
    indicatorLoad.translatesAutoresizingMaskIntoConstraints = NO;
    [NSLayoutConstraint activateConstraints:@[
        [indicatorLoad.centerXAnchor constraintEqualToAnchor:self.contentView.centerXAnchor],
        [indicatorLoad.centerYAnchor constraintEqualToAnchor:self.contentView.centerYAnchor]
    ]];
    
    imageView = [[UIImageView alloc] initWithFrame:self.contentView.frame];
    [imageView setContentMode:UIViewContentModeScaleAspectFit];
    [self.contentView addSubview:imageView];
    
    imageView.translatesAutoresizingMaskIntoConstraints = NO;
    [NSLayoutConstraint activateConstraints:@[
        [imageView.leadingAnchor constraintEqualToAnchor:self.contentView.leadingAnchor],
        [imageView.trailingAnchor constraintEqualToAnchor:self.contentView.trailingAnchor],
        [imageView.topAnchor constraintEqualToAnchor:self.contentView.topAnchor],
        [imageView.bottomAnchor constraintEqualToAnchor:self.contentView.bottomAnchor]
    ]];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    [indicatorLoad startAnimating];
    [imageView setImage:nil];
    [imageView setHidden:YES];
}

@end
