//
//  CollectionCell.h
//  imageD
//
//  Created by Павел Зорин on 24.04.2020.
//  Copyright © 2020 Zorin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionCell : UICollectionViewCell
{
    UIImageView *imageView;
    UIActivityIndicatorView *indicatorLoad;
}
@property UIImageView *imageView;
@property UIActivityIndicatorView *indicatorLoad;
-(void) setupCell;

@end
