//
//  ViewController.m
//  imageD
//
//  Created by Павел Зорин on 23.04.2020.
//  Copyright © 2020 Zorin. All rights reserved.
//

#import "ViewController.h"
#import "CollectionCell.h"
#import "UIImageView+Extension.h"
#import "CacheController.h"
#import "NSMutableArray+Extension.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize collectionView;
@synthesize refreshControll;
@synthesize urlImage;
@synthesize indexImages;
@synthesize cellIdentifier;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    urlImage = [NSURL URLWithString: @"https://picsum.photos/800/600"];
    indexImages = [[NSMutableArray alloc] init];
    [indexImages reloadArray:6];
    cellIdentifier = @"cellIdentifier";
    
    self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    [layout setSectionInset:UIEdgeInsetsMake(10, 10, 10, 10)];
    collectionView=[[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:layout];
    
    [collectionView setDataSource:self];
    [collectionView setDelegate:self];
    
    [collectionView registerClass:[CollectionCell class] forCellWithReuseIdentifier:cellIdentifier];
    [collectionView setBackgroundColor:[UIColor whiteColor]];
    collectionView.showsHorizontalScrollIndicator = NO;
    collectionView.showsVerticalScrollIndicator = NO;
    
    [self.view addSubview:collectionView];
    
    UILayoutGuide*layer = self.view.layoutMarginsGuide;
    if (@available(iOS 11.0, *))
    {
        layer = self.view.safeAreaLayoutGuide;
    }
    
    collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    [NSLayoutConstraint activateConstraints:@[
        [collectionView.leadingAnchor constraintEqualToAnchor:layer.leadingAnchor],
        [collectionView.trailingAnchor constraintEqualToAnchor:layer.trailingAnchor],
        [collectionView.topAnchor constraintEqualToAnchor:layer.topAnchor],
        [collectionView.bottomAnchor constraintEqualToAnchor:layer.bottomAnchor]
    ]];
    
    refreshControll = [[UIRefreshControl alloc] init];
    [refreshControll addTarget:self action:@selector(refreshCollection) forControlEvents:UIControlEventValueChanged];
    
    if (@available(iOS 10.0, *))
    {
        collectionView.refreshControl = refreshControll;
    }
    else
    {
        [self.collectionView addSubview:refreshControll];
    }
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [collectionView.collectionViewLayout invalidateLayout];
}

- (void)refreshCollection {
    [CacheController destroySharedInstance];
    [indexImages reloadArray:6];
    [self.collectionView reloadData];
    [refreshControll endRefreshing];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return indexImages.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    [cell.imageView setUrlImage:urlImage forCache:indexImages[indexPath.row]];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSData*data = [[CacheController sharedInstance] getCacheForKey:indexImages[indexPath.row]];
    if (data == nil) {return;}
    
    UICollectionViewCell *cell=[collectionView cellForItemAtIndexPath:indexPath];
    CGRect frame = cell.contentView.frame;
    
    [UIView animateWithDuration:0.5f animations:^{
        cell.contentView.frame = CGRectOffset(frame, frame.size.width * 2, frame.origin.y);
    } completion:^(BOOL finished) {
        [collectionView performBatchUpdates:^ {
            [self->indexImages removeObject:self->indexImages[indexPath.row]];
            [collectionView deleteItemsAtIndexPaths:@[indexPath]];
        } completion:nil];
    }];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat size = UIScreen.mainScreen.bounds.size.width - 20;
    return CGSizeMake(size, size);
}

@end
