//
//  AppDelegate.h
//  imageD
//
//  Created by Павел Зорин on 23.04.2020.
//  Copyright © 2020 Zorin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    UIWindow *window;
    UIViewController *rootController;
}
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UIViewController *rootController;

@end

