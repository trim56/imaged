//
//  ViewController.h
//  imageD
//
//  Created by Павел Зорин on 23.04.2020.
//  Copyright © 2020 Zorin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
{
    UICollectionView * collectionView;
    UIRefreshControl * refreshControll;
    NSURL * urlImage;
    NSMutableArray * indexImages;
    NSString * cellIdentifier;
}

@property UICollectionView * collectionView;
@property UIRefreshControl * refreshControll;
@property NSURL * urlImage;
@property NSMutableArray * indexImages;
@property NSString * cellIdentifier;

@end

