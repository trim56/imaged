//
//  CacheController.h
//  imageD
//
//  Created by Павел Зорин on 25.04.2020.
//  Copyright © 2020 Zorin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CacheController : NSObject
{    
    NSCache *cache;
}

@property (strong, nonatomic) NSCache *cache;

+(CacheController *) sharedInstance;
+(void) destroySharedInstance;

-(void) setCache:(id)obj forKey:(NSString *)key;
-(id) getCacheForKey:(NSString *)key;

@end
