//
//  UIImageView+Extension.m
//  imageD
//
//  Created by Павел Зорин on 25.04.2020.
//  Copyright © 2020 Zorin. All rights reserved.
//

#import "UIImageView+Extension.h"
#import "CacheController.h"

@implementation UIImageView (Extension)
- (void) setUrlImage:(NSURL *) url forCache:(NSString*) index {
    
    NSData*data = [[CacheController sharedInstance] getCacheForKey:index];
    if (data != nil)
    {
        [self setImage:[UIImage imageWithData:data]];
        [self setHidden:NO];
    }
    else
    {
        [self getData:url completion:^(NSData * _Nonnull data, NSURLResponse * _Nonnull response, NSError * _Nonnull error)
        {
            UIImage*image = nil;
            if (error == nil && data != nil)
            {
                image = [UIImage imageWithData:data];
                [[CacheController sharedInstance] setCache:data forKey:index];
            }
            else
            {
                image = [UIImage imageNamed:@"notFound"];
            }
            dispatch_async(dispatch_get_main_queue(), ^(void)
            {
                [self setImage:image];
                [self setHidden:NO];
            });
        }];
    }
}

- (void) getData: (NSURL*) url completion:(void(^)(NSData *data, NSURLResponse *response, NSError *error))callback
{
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithURL:url completionHandler:callback];
    [task resume];
}
@end
