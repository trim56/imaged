//
//  NSMutableArray+Extension.m
//  imageD
//
//  Created by Павел Зорин on 26.04.2020.
//  Copyright © 2020 Zorin. All rights reserved.
//

#import "NSMutableArray+Extension.h"

@implementation NSMutableArray (Extension)

- (void) reloadArray:(NSUInteger) count {
    [self removeAllObjects];
    for (NSUInteger i = 1; i <= count; i++)
    {
        [self addObject:[NSString stringWithFormat:@"%li",  i+1]];
    }
}

@end

