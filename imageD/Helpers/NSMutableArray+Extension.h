//
//  NSMutableArray+Extension.h
//  imageD
//
//  Created by Павел Зорин on 26.04.2020.
//  Copyright © 2020 Zorin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSMutableArray (Extension)
- (void) reloadArray:(NSUInteger) count;
@end

NS_ASSUME_NONNULL_END
