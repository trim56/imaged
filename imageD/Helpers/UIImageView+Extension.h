//
//  UIImageView+Extension.h
//  imageD
//
//  Created by Павел Зорин on 25.04.2020.
//  Copyright © 2020 Zorin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImageView (Extension)
- (void) setUrlImage:(NSURL*) url forCache:(NSString*) index;
- (void) getData: (NSURL*) url completion:(void(^)(NSData *data, NSURLResponse *response, NSError *error))callback;
@end

NS_ASSUME_NONNULL_END
